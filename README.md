# Generate secret from authentication


```bash
htpasswd -c users admin
kubectl create secret generic admin-authsecret --from-file=users -n traefik
```
